<?php

/**
 * @file
 * Include file for helper functions.
 */

/**
 * Helper function to properly format query string parameter/value pairs.
 *
 * Strip the [OPERATOR] from the supplied value and append it to the parameter's
 * key.
 *
 * @param array &$parameters
 *    The array of parameters, passed by reference so we can modify it.
 * @param string &$param
 *    The key of the parameter to be reformated.
 * @param int|string &$value
 *    The value of the parameter to be reformated.
 */
function _govtrack_fix_operator(array &$parameters, &$param, &$value) {
  // Split the value, but keep the delimiters.
  $pieces = preg_split('/(_)/', $value, -1, PREG_SPLIT_DELIM_CAPTURE);
  $el_count = count($pieces);
  // The last 2 pieces of the array will combine to form the
  // [OPERATOR].
  $operator = $pieces[$el_count - 2] . $pieces[$el_count - 1];
  if (_govtrack_check_operator($operator)) {
    $new_param = "{$param}_{$operator}";
    $new_value = preg_replace("/$operator/", '', $value);
    $parameters[$new_param] = $new_value;
    unset($parameters[$param]);
    // Make sure to change the original parameter name and value so the
    // [SORTABLE] function operates on the correct parameter index.
    $param = $new_param;
    $value = $new_value;
  }
}

/**
 * Helper function to check validity of supplied [OPERATOR].
 *
 * @param string $value
 *    The value to be checked.
 *
 * @return bool
 *    Whether $value is valid or not.
 */
function _govtrack_check_operator($value) {
  $valid_operators = unserialize(GOVTRACK_OPERATORS);

  $valid = FALSE;
  foreach ($valid_operators as $operator) {
    if ($value === $operator) {
      $valid = TRUE;
      break;
    }
  }
  return $valid;
}

/**
 * Helper function to find [OPERATORS] in a string.
 *
 * @param string $value
 *    The value to inspect.
 *
 * @return bool
 *    Whether $value was matched or not.
 */
function _govtrack_find_operators($value) {
  $match = FALSE;
  $valid_operators = unserialize(GOVTRACK_OPERATORS);
  foreach ($valid_operators as $operator) {
    if (strpos($value, $operator)) {
      $match = TRUE;
      break;
    }
  }
  return $match;
}

/**
 * Helper function to process parameters.
 *
 * Unset any null parameters.
 * Identify/handle [OPERATORS].
 *
 * @param array &$parameters
 *    The array of parameters passed to the calling function.
 */
function _govtrack_process_parameters(array &$parameters) {
  foreach ($parameters as $param => $val) {
    if (is_null($val)) {
      // Remove any null parameters from the array to prevent null values in the
      // query string.
      unset($parameters[$param]);
    }

    // Find [OPERATORS], if present.
    if (_govtrack_find_operators($val)) {
      // Fix the format of parameter/value pairs if an [OPERATOR] is being
      // applied.
      _govtrack_fix_operator($parameters, $param, $val);
    }
  }
}

/**
 * Helper function to handle HTTP requests.
 *
 * Perform the HTTP request and return the results. If there are errors, log
 * them.
 *
 * @param array $parameters
 *    The array containing the query parameters we wish to add to the uri.
 * @param string $uri
 *    The uri we wish to request.

 * @return string
 *    The results of the http_request: govtrack data if successful, an error
 *    message if not successful.
 */
function _govtrack_handle_http_request(array $parameters, $uri) {
  // Create the url.
  $full_url = url($uri, array('query' => $parameters));

  $cache_params = implode('-', $parameters);
  $cache_id = 'govtrack_' . $uri . '_' . $cache_params;
  $mem_cache_result = cache_get($cache_id);
  if (empty($mem_cache_result->data)) {
    $object = drupal_http_request($full_url);
    cache_set($cache_id, $object, 'cache', time() + 3600);
  }
  else {
    $object = $mem_cache_result->data;
  }

  if (isset($object->error)) {
    watchdog('govtrack', "$object->error: $object->data");
  }
  return $object;
}

/**
 * Helper function to merge global and local parameters.
 *
 * @param array $defaults
 *    The array of local parameter defaults.
 *
 * @return array
 *    Return the merged parameter array.
 */
function _govtrack_merge_default_parameters(array $defaults) {
  return array_merge($defaults, unserialize(GOVTRACK_GLOBAL_PARAMS));
}

/**
 * Helper function to handle the 'id' parameter.
 *
 * Depending on the type of request, the 'id' parameter may be part of the
 * provided query and/or it may be appended to the base URI ... OR, it may only
 * be appended to the base URI and removed from the query.
 *
 * @param array &$parameters
 *    An array of optional parameters.
 * @param string &$uri
 *    The URI to be requested.
 */
function _govtrack_handle_ID(array &$parameters, &$uri) {

  $nonfilterable_ids = array(
    'bill',
    'person',
    'role',
  );

  $uri_components = explode('/', $uri);
  $request_type = $uri_components[5];

  // Nonfilterable ID:
  if (in_array($request_type, $nonfilterable_ids)) {
    // The 'id' parameter is NOT part of the query string...
    // if this parameter is set, append it to the end of the
    // uri and unset it from the parameter array.
    if (!is_null($parameters['id'])) {
      $id = $parameters['id'];
      unset($parameters['id']);
      $uri .= "/{$id}";
    }
  }
  // Filterable ID:
  else {
    if (preg_match('/uri\+/', $parameters['id'])) {
      $parameters['id'] = preg_replace('/uri\+/', '', $parameters['id']);
      $uri .= "/{$parameters['id']}";
    }
    elseif (preg_match('/uri\-/', $parameters['id'])) {
      $parameters['id'] = preg_replace('/uri\-/', '', $parameters['id']);
      $uri .= "/{$parameters['id']}";
      unset($parameters['id']);
    }
  }
}
